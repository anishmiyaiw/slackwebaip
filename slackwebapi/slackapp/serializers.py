from django.contrib.auth.models import User
from rest_framework import serializers
from slackapp.models import Member


class MemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = Member
        fields = ('id', 'account', 'billing', 'authentication', 'email', 'user')


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username',)
