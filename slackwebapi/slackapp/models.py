from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Member(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    account = models.CharField(max_length=25)
    billing = models.CharField(max_length=25)
    authentication = models.CharField(max_length=25)
    email = models.EmailField()

