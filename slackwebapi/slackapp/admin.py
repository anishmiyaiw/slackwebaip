from django.contrib import admin

# Register your models here.
from slackapp.models import Member

admin.site.register(Member)
