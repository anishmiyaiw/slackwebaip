from django.contrib.auth.models import User
from rest_framework import viewsets, permissions

from slackapp.models import Member
from slackapp.serializers import MemberSerializer, UserSerializer


class MemberViewSet(viewsets.ModelViewSet):
    queryset = Member.objects.all()
    serializer_class = MemberSerializer


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
